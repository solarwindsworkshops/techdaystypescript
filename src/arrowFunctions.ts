{
    class User {
        constructor(public age: number) { }

        // growOld() {
        //     this.age++;
        // }

        growOld = () => {
            this.age++;
        }
    }

    var user = new User(1);
    setTimeout(user.growOld, 1000);

    setTimeout(function () { console.log(user.age); }, 2000);
}