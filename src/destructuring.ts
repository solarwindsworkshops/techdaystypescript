{
    //object destructuring
    let circle = {
        x: 10,
        y: 30,
        radius: 50
    }

    let {x: positionX, y, radius} = circle;

    console.log("x: " + positionX);
    console.log("y: " + y);
    console.log("radius: " + radius);


    //array destructuring
    let numbers = [1, 2, 3];

    let [x1, y1] = numbers;

    console.log(x1);
    console.log(y1);

    //array destructuring with ignore
    let [first, , third] = numbers;
    console.log(first);
    console.log(third);
}