{
    let someNumber = 7;

    if (true) {
        let someNumber = 45;
    }

    console.log(someNumber);


    //closures
    var funcs = [];
    for (let i = 0; i < 3; i++) {
        funcs.push(() => { console.log(i) });
    }

    for (var j = 0; j < 3; j++) {
        funcs[j]();
    }
}