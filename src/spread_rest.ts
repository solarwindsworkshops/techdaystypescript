{
    class Rest {
        takeItAll = (first, second, ...other) => {

            console.log(other);
        }
    }

    let rest = new Rest();
    rest.takeItAll("a", "b", "c", "d", "e");


    //rest used in destructuring
    let numbers = [4, 5, 78, 6, 4];

    let [head, ...restOfNumbers] = numbers;

    console.log("head: " + head);
    console.log("rest: " + restOfNumbers);

    for (let item of restOfNumbers) {
        console.log(item);
    }

    //combining two tables
    let list = [1, 2];
    list = [...list, 3, 4];

    class Foo {
        public name: string;
    }

    //copy objects
    let foo = new Foo();
    foo.name = "John";

    let bar = { ...foo };

    console.log("bar name: " + bar.name);
}